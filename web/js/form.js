/**
 * Created by MCE-PC on 06-Jun-17.
 */
$(document).ready(function() {
    $("#users-rights").select2();
    $('.datepicker').datepicker({
        viewMode: 'years',
        format: 'yyyy-mm-dd',
        autoclose: true
    });
    $("#feedback-outlet_id").change(function () {
        var val=$("#feedback-outlet_id option:selected").val();
        var url=$(this).attr('data-url');
        $.ajax({
            type: "POST",
            url: url,
            data: {id:val},
            success: function(data){
                var res = $.parseJSON(data);
                if(res.status==1){
                    $('#feedback-customer_id').html(res.html);
                }
                else{
                    //alert(res.msg);
                }

            }
        });

    });
    $( '[data-role="ratingbar"]' )
        .ratingbar()
        .click(function() {
            // Grab value
            $('#ratestar').val($( this ).attr( 'data-value' ));

            return false;
        });
    var options   = {},
        selector  = 'input[type="checkbox"][data-widget="bootstrap-switch"]',
        activator = function () { $(this).bootstrapSwitch(options); };

    $(selector).each(activator);
});
function getcust(v){
    var val = $(v).val();
    var url=$(v).attr('data-url');
    $.ajax({
        type: "POST",
        url: url,
        data: {num:val},
        success: function(data){
            var res = $.parseJSON(data);
            if(res.status==1){
                $('#feedback-customer_id').html(res.html);
            }
            else{
                //alert(res.msg);
            }

        }
    });
}