<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'https://cdn.datatables.net/v/dt/dt-1.10.18/datatables.min.css',
        'https://afeld.github.io/emoji-css/emoji.css',
        'css/site.css',
        'css/font-awesome.min.css',
        'css/print.css',
        'css/style.css',
        'css/select2-bootstrap.css',
        'css/select2.css',
        'css/datepicker3.css',
        'css/bootstrap-switch.min.css',
        'css/ratingbar.css',
		'frontend/elisyam/vendors/css/base/responsive.css',
    ];
    public $js = [

        'https://cdn.datatables.net/v/dt/dt-1.10.18/datatables.min.js',
        'https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js',
        'https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js',
        'https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js',
        'https://cdn.datatables.net/buttons/1.1.2/js/buttons.flash.js',
        'https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js',
        'https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js',

        'js/bootstrap-datepicker.js',
        'js/select2.min.js',
        'js/ratingbar.js',
        'js/bootstrap-switch.js',
        'js/form.js',
        
    ];
    public $depends = [

        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
