<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class FrontEndAppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'frontend/elisyam/vendors/css/base/datepicker.css',
        //'https://cdnjs.cloudflare.com/ajax/libs/datepicker/0.6.5/datepicker.css',
        'frontend/elisyam/vendors/css/base/bootstrap.min.css',
        'frontend/elisyam/vendors/css/base/emoji.css',
        //'https://afeld.github.io/emoji-css/emoji.css',
        'frontend/elisyam/vendors/css/base/elisyam-1.5.min.css',
        'frontend/elisyam/vendors/css/base/csshake.min.css',
        'frontend/elisyam/css/bootstrap-select/bootstrap-select.min.css',
        'frontend/elisyam/css/animate/animate.min.css',
        'frontend/elisyam/css/datatables/datatables.min.css',
        'frontend/css/site.css',
        'frontend/css/csshake.min.css',
        'frontend/css/faceMocion.css',
        //'frontend/elisyam/vendors/css/base/font-awesome.min.css',
        'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css',
		'frontend/elisyam/vendors/css/base/responsive.css',

    ];
    public $js = [
        'frontend/elisyam/vendors/js/base/jquery.validate.min.js',
        'frontend/elisyam/vendors/js/base/datepicker.js',
        //'http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js',
        //'https://cdnjs.cloudflare.com/ajax/libs/datepicker/0.6.5/datepicker.js',
        'frontend/elisyam/vendors/js/base/emoji.js',
        'frontend/elisyam/vendors/js/base/emoji.min.js',
        'frontend/elisyam/vendors/js/base/core.min.js',
        'frontend/elisyam/vendors/js/nicescroll/nicescroll.min.js',
        //'elisyam/vendors/js/waypoints/waypoints.min.js',
        'frontend/elisyam/vendors/js/progress/circle-progress.min.js',
        'frontend/elisyam/vendors/js/bootstrap-wizard/bootstrap.wizard.min.js',
        'frontend/elisyam/vendors/js/app/app.min.js',
        'frontend/elisyam/vendors/js/bootstrap-select/bootstrap-select.min.js',
        'frontend/elisyam/vendors/js/datatables/datatables.min.js',
        //'elisyam/js/dashboard/db-modern.min.js',
        'frontend/elisyam/js/custom.js',
        'frontend/elisyam/js/bootbox.min.js',
        'frontend/elisyam/js/components/wizard/form-wizard.min.js',

        'frontend/elisyam/js/components/validation/validation.min.js',
        'frontend/css/emoji.min.js',
        'frontend/js/faceMocion.js',
        'frontend/js/custom.js',



    ];
    public $depends = [
        #'yii\web\YiiAsset',
        #'yii\bootstrap\BootstrapAsset',
    ];
}
