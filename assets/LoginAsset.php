<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class LoginAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'frontend/elisyam/vendors/css/base/bootstrap.min.css',
        'frontend/elisyam/vendors/css/base/elisyam-1.5.min.css',
        'frontend/css/site.css',
    ];
    public $js = [

        'frontend/elisyam/vendors/js/base/core.min.js',
        'frontend/elisyam/vendors/js/app/app.min.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
