<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use app\models\users;
use app\models\usersSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\AuthItem;

/**
 * UsersController implements the CRUD actions for users model.
 */
class UsersController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all users models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new usersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single users model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new users model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new users();

        if ($model->load(Yii::$app->request->post())) {
           // echo '<pre>'; print_r($_POST);exit;
            $model->rights = $_POST['Users']['rights'];
            $model->created_by = Yii::$app->user->identity->id;
            $model->updated_by = Yii::$app->user->identity->id;
            $model->authKey = '-';
            $model->created_on = date('Y-m-d h:i:s');
            $model->updated_on = date('Y-m-d h:i:s');
            if($model->save()){
                $model->authKey = $model->id.'-'.$model->user;
                if($model->save()){
                    // the following three lines were added:
                    if(!empty($model->rights)) {
                        $auth = \Yii::$app->authManager;
                        $authorRole = $auth->getRole($model->rights);
                        $auth->assign($authorRole, $model->id);
                    }
                    return $this->redirect(['view', 'id' => $model->id]);

                }
            }
            else{
                print_r($model->getErrors());exit;
            }
        }
            return $this->render('create', [
                'model' => $model,
            ]);

    }

    /**
     * Updates an existing users model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $oldright = $model->rights;
        if ($model->load(Yii::$app->request->post())) {
            $model->rights = $_POST['Users']['rights'];
            if($model->save()) {
                if (!empty($_POST['Users']['rights']) && $oldright != $_POST['Users']['rights']) {
                    $auth = \Yii::$app->authManager;
                    $authorRole = $auth->getRole($model->rights);
                    $auth->assign($authorRole, $model->id);
                }
                return $this->redirect(['index']);
            }
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing users model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the users model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return users the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = users::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
