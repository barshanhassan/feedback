<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "users".
 *
 * @property integer $id
 * @property string $name
 * @property string $email
 * @property string $authKey
 * @property string $user
 * @property string $password
 * @property integer $status
 * @property string $rights
 * @property integer $outlet_id
 * @property integer $created_by
 * @property string $created_on
 * @property integer $updated_by
 * @property integer $updated_on
 *
 * @property Outlet $outlet
 */
class Users extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'user', 'password','outlet_id','rights'], 'required'],
            [['status', 'outlet_id', 'created_by', 'updated_by', 'updated_on'], 'integer'],
            [['created_on'], 'safe'],
            [['name', 'password', 'rights'], 'string', 'max' => 50],
            [['email'], 'string', 'max' => 100],
            [['authKey'], 'string', 'max' => 10],
            [['user'], 'string', 'max' => 20],
            [['outlet_id'], 'exist', 'skipOnError' => true, 'targetClass' => Outlet::className(), 'targetAttribute' => ['outlet_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'email' => Yii::t('app', 'Email'),
            'authKey' => Yii::t('app', 'Auth Key'),
            'user' => Yii::t('app', 'User'),
            'password' => Yii::t('app', 'Password'),
            'status' => Yii::t('app', 'Status'),
            'rights' => Yii::t('app', 'Rights'),
            'outlet_id' => Yii::t('app', 'Outlet ID'),
            'created_by' => Yii::t('app', 'Created By'),
            'created_on' => Yii::t('app', 'Created On'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'updated_on' => Yii::t('app', 'Updated On'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOutlet()
    {
        return $this->hasOne(Outlet::className(), ['id' => 'outlet_id']);
    }

    /**
     * @inheritdoc
     * @return UsersQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new UsersQuery(get_called_class());
    }
}
