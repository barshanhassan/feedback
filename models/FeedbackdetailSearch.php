<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Feedbackdetail;

/**
 * FeedbackdetailSearch represents the model behind the search form about `app\models\Feedbackdetail`.
 */
class FeedbackdetailSearch extends Feedbackdetail
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'feedback_id', 'categoryid', 'subcategoryid', 'reviewtypeid'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Feedbackdetail::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'feedback_id' => $this->feedback_id,
            'categoryid' => $this->categoryid,
            'subcategoryid' => $this->subcategoryid,
            'reviewtypeid' => $this->reviewtypeid,
        ]);

        return $dataProvider;
    }
}
