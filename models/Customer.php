<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "customer".
 *
 * @property integer $id
 * @property string $name
 * @property integer $contact_no
 * @property string $birthdate
 * @property string $city
 * @property string $email
 * @property integer $sms_promotion
 * @property integer $link_to_facebook
 * @property string $facebook_link
 * @property integer $status
 * @property integer $outlet_id
 * @property string $created_on
 * @property integer $created_by
 * @property string $updated_on
 * @property integer $updated_by
 *
 * @property Outlet $outlet
 * @property Feedback[] $feedbacks
 */
class Customer extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'customer';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'outlet_id'], 'required'],
            [['sms_promotion', 'link_to_facebook', 'status', 'outlet_id', 'created_by', 'updated_by'], 'integer'],
            [['birthdate', 'created_on', 'updated_on'], 'safe'],
            [['name', 'city', 'email', 'facebook_link'], 'string', 'max' => 100],
            [['outlet_id'], 'exist', 'skipOnError' => true, 'targetClass' => Outlet::className(), 'targetAttribute' => ['outlet_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'contact_no' => Yii::t('app', 'Contact No'),
            'birthdate' => Yii::t('app', 'Birthdate'),
            'city' => Yii::t('app', 'City'),
            'email' => Yii::t('app', 'Email'),
            'sms_promotion' => Yii::t('app', 'Sms Promotion'),
            'link_to_facebook' => Yii::t('app', 'Link To Facebook'),
            'facebook_link' => Yii::t('app', 'Facebook Link'),
            'status' => Yii::t('app', 'Status'),
            'outlet_id' => Yii::t('app', 'Outlet ID'),
            'created_on' => Yii::t('app', 'Created On'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_on' => Yii::t('app', 'Updated On'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOutlet()
    {
        return $this->hasOne(Outlet::className(), ['id' => 'outlet_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFeedbacks()
    {
        return $this->hasMany(Feedback::className(), ['customer_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return CustomerQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CustomerQuery(get_called_class());
    }


}
