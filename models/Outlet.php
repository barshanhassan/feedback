<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "outlet".
 *
 * @property integer $id
 * @property string $name
 * @property string $address
 * @property string $contactno
 * @property string $email
 * @property integer $status
 *
 * @property Users[] $users
 */
class Outlet extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'outlet';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'address', 'contactno'], 'required'],
            [['status'], 'integer'],
            [['name', 'contactno', 'email'], 'string', 'max' => 100],
            [['address'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'address' => Yii::t('app', 'Address'),
            'contactno' => Yii::t('app', 'Contactno'),
            'email' => Yii::t('app', 'Email'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(Users::className(), ['outlet_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return OutletQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new OutletQuery(get_called_class());
    }
}
