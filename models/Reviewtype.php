<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "reviewtype".
 *
 * @property integer $id
 * @property string $name
 * @property integer $status
 */
class Reviewtype extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'reviewtype';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['status'], 'integer'],
            [['name'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

    /**
     * @inheritdoc
     * @return ReviewtypeQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ReviewtypeQuery(get_called_class());
    }
}
