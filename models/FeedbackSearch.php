<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Feedback;
use DateTime;

/**
 * FeedbackSearch represents the model behind the search form about `app\models\Feedback`.
 */
class FeedbackSearch extends Feedback
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'outlet_id','customer_id', 'created_by', 'updated_by'], 'integer'],
            [['overall_rating', 'remarks', 'created_on', 'updated_on'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Feedback::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }


        if(Yii::$app->user->identity->outlet_id==1){
            $query->andFilterWhere([
                'outlet_id' => $this->outlet_id,
            ]);
        }
        else{
            $query->andFilterWhere([
                'outlet_id' => Yii::$app->user->identity->outlet_id,
            ]);
        }


        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
           // 'outlet_id' => $this->outlet_id,
            'customer_id' => $this->customer_id,
            //'created_on' => $this->created_on,
            'created_by' => $this->created_by,
            'updated_on' => $this->updated_on,
            'updated_by' => $this->updated_by,
        ]);
        if($this->created_on)
        {
            $mysql_date = $this->created_on;
            // date in Y-m-d format as MySQL stores it
            $date_obj = date_create_from_format('d-m-Y',$mysql_date);
            $created_on = date_format($date_obj, 'Y-m-d');
            $query->andFilterWhere(['like', 'created_on', $created_on]);
        }
        $query->andFilterWhere(['like', 'overall_rating', $this->overall_rating])
            ->andFilterWhere(['like', 'remarks', $this->remarks]);
        return $dataProvider;
    }

    public function report($params)
    {





        if($params['FeedbackSearch']['created_on'])
        {
            $query = Feedback::find()->joinWith('feedBackDetail');

        }else {
            $start_date = date('Y-m-d');
            $query = Feedback::find()->joinWith('feedBackDetail')->andWhere(['like', 'created_on', $start_date]);
        }


        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }


        if ( ! is_null($this->created_on) && strpos($this->created_on, ' - ') !== false ) {
            list($start_date, $end_date) = explode(' - ', $this->created_on);
            $start_date = DateTime::createFromFormat('d/m/Y', $start_date);
            $start_date = $start_date->format('Y-m-d');
            $start_date = $start_date.' 00:00:00';

            $end_date = DateTime::createFromFormat('d/m/Y', $end_date);
            $end_date = $end_date->format('Y-m-d');
            $end_date = $end_date.' 23:59:59';
            $query->andFilterWhere(['between', 'created_on', $start_date, $end_date]);

        }

        $query->andFilterWhere([
            'outlet_id' => $this->outlet_id,
        ]);


        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            // 'outlet_id' => $this->outlet_id,
            'customer_id' => $this->customer_id,
            //'created_on' => $this->created_on,
            'created_by' => $this->created_by,
            'updated_on' => $this->updated_on,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'overall_rating', $this->overall_rating])
            ->andFilterWhere(['like', 'remarks', $this->remarks]);

        return $dataProvider;

    }
}
