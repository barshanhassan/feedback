<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\OutletSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Outlets');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="outlet-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Outlet'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
/*
            'id',*/
            'name',
            'address',
            'contactno',
           // 'email:email',
            [
                'attribute' => 'status',
                'header' =>'Status',
                'value' =>function($data){
                    if($data['status']==1){
                        return 'Active';
                    }
                    else
                        return 'Inactive';
                },
                'filter'=>array('1'=>'Active','0'=>'Inactive')

            ],

            //'status',

            ['class' => 'yii\grid\ActionColumn',
                'header'=>'Actions',
                'visibleButtons' => [


                    'delete' => function ($model) {
                        return $model->id == 0;
                    },

                ]
            ],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
