<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Feedback */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="feedback-form">


    <div class="row col-md-8 col-md-offset-2">
        <div class="col-lg-12">
            <table class="table table-bordered table-responsive table-condensed">
                <thead>
                    <tr>
                        <th>Complaint Category</th>
                        <th>&nbsp;</th>
                        <?php $revtype = \app\models\Reviewtype::find()->where('status=1')->all(); //echo '<pre>'; print_r($revtype);
                        foreach($revtype as $v){
                            ?>
                            <th><?= $v->name?></th>
                        <?php
                        }
                        ?>
                    </tr>
                </thead>
                <tbody>
                <?php $cat = \app\models\Category::find()->where('status=1')->all();
                foreach($cat as $k=>$v){
                    $subcat = \app\models\Subcategory::find()->where('categoryid = '.$v->id.' and status=1')->all();
                    ?>

                <?php foreach($subcat as $d=>$p){ ?>
                    <tr>
                        <?php if($subcat[$d]['categoryid']!=$subcat[$d-1]['categoryid']){
                            ?>
                            <td class="centercontent" rowspan="<?= count($subcat)?>"><?= $v->cat_name ?></td>
                        <?php
                        } ?>
                            <td><?= $p->name ?></td>
                        <?php $revtype = \app\models\Reviewtype::find()->where('status=1')->all(); //echo '<pre>'; print_r($revtype);
                        foreach($revtype as $m){
                            ?>
                            <td>
                                <div class="radio">
                                    <label><input type="radio" id='subcatradio_<?= $v->id.'-'.$p->id.'-'.$m->id ?>' value="<?= $v->id.'-'.$p->id.'-'.$m->id ?>" name="subcatradio[<?= $p->id ?>]"></label>
                                </div>
                            </td>
                        <?php
                        }
                        ?>
                    </tr>
                <?php } ?>
                <?php
                }
                ?>
                </tbody>
            </table>
        </div>
        <div class="col-lg-12">
            <table id="setupcust" class="table table-bordered table-responsive table-condensed">
                <tbody>
                <tr>
                    <th><b>Any Other Remarks</b></th>
                    <td style="padding: 0;"><input placeholder="Add Remarks" style="border-radius: 0;border: none;box-shadow: none;" type="text" id="feedback-remarks" class="form-control" name="remarks" maxlength="255" aria-invalid="false"></td>
                </tr>
                <tr>
                    <th><b>Visited Outlet</b></th>
                    <td style="padding: 0;"><input value="<?= \app\models\Outlet::findOne(Yii::$app->user->identity->outlet_id)->name?>" disabled="disabled" style="border-radius: 0;border: none;box-shadow: none;" type="text" id="feedback-remarks" class="form-control" name="remarks" maxlength="255" aria-invalid="false"></td>
                </tr>
                <tr>
                    <th><b>Overall Visit Review (/5)</b></th>
                    <td style="padding: 0;text-align: center;">
                        <div data-role="ratingbar" data-steps="3" style="font-size: 10px">
                            <ul>
                                <li><a href="#"><span class="glyphicon glyphicon-star"></span></a></li>
                                <li><a href="#"><span class="glyphicon glyphicon-star"></span></a></li>
                                <li><a href="#"><span class="glyphicon glyphicon-star"></span></a></li>
                                <li><a href="#"><span class="glyphicon glyphicon-star"></span></a></li>
                                <li><a href="#"><span class="glyphicon glyphicon-star"></span></a></li>
                            </ul>
                        </div>
                        <input type="hidden" id="ratestar" name="ratestar">
                    </td>
                </tr>
                <tr>
                    <th><b>SMS Promotions (Offers)</b></th>
                    <td style="text-align: center;">
                        <input id="smspromo" type="checkbox" value="yes" data-on-text="Yes" data-off-text="No" name="smspromo" data-widget="bootstrap-switch"></td>
                </tr>
                <tr>
                    <th><b>Link to Facebook</b></th>
                    <td style="text-align: center;">
                        <input id="fbpromo" type="checkbox" data-on-text="Yes" data-off-text="No" value="yes" name="fbpromo" data-widget="bootstrap-switch"></td>
                </tr>
                </tbody>
            </table>
        </div>

        <div class="col-lg-12">
            <blockquote class="blockquote btn-info">
                <p class="mb-0">Customer Details</p>
            </blockquote>
            <table id="custform" class="table table-bordered table-responsive table-condensed">
                <tbody>
                <tr>
                    <th><b>Name</b></th>
                    <td style="padding: 0;"><input placeholder="Customer Name" style="border-radius: 0;border: none;box-shadow: none;" type="text" id="customer-name" class="form-control" name="customer-name" maxlength="255" aria-invalid="false"></td>
                </tr>
                <tr>
                    <th><b>Contact/Cell#</b></th>
                    <td style="padding: 0;"><input data-url='<?= \Yii::$app->urlManager->createUrl(['site/getcustdetail'])?>' onchange="getcust(this);" placeholder="Customer Contact"  style="border-radius: 0;border: none;box-shadow: none;" type="text" id="customer-contact" class="form-control" name="customer-contact" maxlength="255" aria-invalid="false"></td>
                </tr>
                <tr>
                    <th><b>Email</b></th>
                    <td style="padding: 0;"><input  placeholder="Customer Email" style="border-radius: 0;border: none;box-shadow: none;" type="text" id="customer-email" class="form-control" name="customer-email" maxlength="255" aria-invalid="false"></td>
                </tr>
                <tr>
                    <th><b>Birthday</b></th>
                    <td style="padding: 0;"><input  placeholder="Customer Birthday" style="border-radius: 0;border: none;box-shadow: none;" type="text" id="customer-birthday" class="form-control" name="customer-birthday" maxlength="255" aria-invalid="false"></td>
                </tr>
                <tr>
                    <th><b>City</b></th>
                    <td style="padding: 0;"><input  placeholder="Customer City" style="border-radius: 0;border: none;box-shadow: none;" type="text" id="customer-city" class="form-control" name="customer-city" maxlength="255" aria-invalid="false"></td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>

</div>
<style>
    .radio label, .checkbox label {
        width: 100%;
        height: 100%;
    }
</style>