<div class="feedback-form">
    <div class="row col-md-8 col-md-offset-2">
        <div class="col-lg-12">
            <table class="table table-bordered table-responsive table-condensed">
                <thead>

                <tr>

                    <th>Detail&nbsp;</th>
                    <?php $revtype = \app\models\Reviewtype::find()->where('status=1')->all(); //echo '<pre>'; print_r($revtype);
                    foreach($revtype as $v){
                        ?>
                        <!--<th><?php $v->name ?></th>-->
                        <?php
                    }
                    ?>
                    <td><i class="em em-slightly_smiling_face" style="width: 40px; height: 50px; background-image:url('<?= Yii::$app->homeUrl?>frontend/excellent.png');"></i></td>
                    <td><i class="em em-full_moon_with_face" style="width: 40px; height: 50px; background-image:url('<?= Yii::$app->homeUrl?>frontend/good.png');"></i></td>
                    <td><i class="em em-angry" style="width: 40px; height: 50px; background-image:url('<?= Yii::$app->homeUrl?>frontend/fair.png');"></i></td>
                </tr>
                </thead>
                <tbody>
                <?php $cat = \app\models\Category::find()->where('status=1')->all();
                $detail = \app\models\Feedbackdetail::find()->where(['feedback_id'=>$model->id])->all();
                foreach($cat as $k=>$v){
                    $subcat = \app\models\Subcategory::find()->where('categoryid = '.$v->id.' and status=1')->all();
                    ?>

                    <?php foreach($subcat as $d=>$p){ ?>
                        <tr>
                            <?php if($subcat[$d]['categoryid']!=$subcat[$d-1]['categoryid']){
                                ?>

                                <?php
                            } ?>
                            <td><?= $p->name ?></td>
                            <?php $revtype = \app\models\Reviewtype::find()->where('status=1')->all(); //echo '<pre>'; print_r($revtype);
                            foreach($revtype as $m){
                                $detail = \app\models\Feedbackdetail::find()->where(['feedback_id'=>$model->id,'categoryid'=>$v->id,'subcategoryid'=>$p->id,'reviewtypeid'=>$m->id])->one();

                                if($detail)
                                {
                                    $checked = 'checked';
                                }else {
                                    $checked = '';
                                }
                                
                                ?>
                                <td>
                                    <div class="radio">
                                        <label><input type="radio" id='subcatradio_<?= $v->id.'-'.$p->id.'-'.$m->id ?>' value="<?= $v->id.'-'.$p->id.'-'.$m->id ?>" name="subcatradio[<?= $p->id ?>]" <?=$checked?> disabled></label>
                                    </div>
                                </td>
                                <?php
                            }
                            ?>
                        </tr>
                    <?php } ?>
                    <?php
                }
                ?>
                </tbody>
            </table>
        </div>
        <div class="col-lg-12">
            <table id="setupcust" class="table table-bordered table-responsive table-condensed">
                <tbody>
                <tr>
                    <th><b>Any Other Remarks</b></th>
                    <td style="padding: 0;"><input placeholder="Add Remarks" style="border-radius: 0;border: none;box-shadow: none;" type="text" id="feedback-remarks" class="form-control" value="<?=$model->remarks?>" name="remarks" maxlength="255" aria-invalid="false" disabled></td>
                </tr>
                <tr>
                    <th><b>Visited Outlet</b></th>
                    <td style="padding: 0;"><input value="<?= \app\models\Outlet::findOne($model->outlet_id)->name?>" disabled="disabled" style="border-radius: 0;border: none;box-shadow: none;" type="text" id="feedback-remarks" class="form-control" name="remarks" maxlength="255" aria-invalid="false" ></td>
                </tr>

                <tr>
                    <th><b>Overall Visit Review (/5)</b></th>
                    <td style="padding: 0;text-align: center;">
                        <div data-role="ratingbar" data-steps="3" style="font-size: 10px " data-value="5" class="star-ctr">

                            <?php
                            $rate=\app\models\Feedback::findOne($model->id)->overall_rating*33;

                            ?>

                            <ul style="width: <?=$rate?>px;" class="star-fg">
                                <li><a href="#"><span class="glyphicon glyphicon-star"></span></a></li>
                                <li><a href="#"><span class="glyphicon glyphicon-star"></span></a></li>
                                <li><a href="#"><span class="glyphicon glyphicon-star"></span></a></li>
                                <li><a href="#"><span class="glyphicon glyphicon-star"></span></a></li>
                                <li><a href="#"><span class="glyphicon glyphicon-star"></span></a></li>

                            </ul></div>
                        <input type="hidden" id="ratestar" name="ratestar">
                    </td>
                </tr>
                <?php
                $customer = \app\models\Customer::findOne($model->customer_id);


                ?>

                </tbody>
            </table>
        </div>

        <div class="col-lg-12">
            <blockquote class="blockquote btn-info">
                <p class="mb-0">Customer Details</p>
            </blockquote>
            <table id="custform" class="table table-bordered table-responsive table-condensed">
                <tbody>
                <tr>
                    <th><b>Name</b></th>
                    <td style="padding: 0;"><input placeholder="Customer Name" style="border-radius: 0;border: none;box-shadow: none;" type="text" id="customer-name" value="<?=$model->customer->name?>" class="form-control" name="customer-name" maxlength="255" aria-invalid="false" disabled></td>
                </tr>
                <tr>
                    <th><b>Contact/Cell#</b></th>
                    <td style="padding: 0;"><input  placeholder="Customer Contact"  style="border-radius: 0;border: none;box-shadow: none;" type="text" id="customer-contact" value="<?=$model->customer->contact_no?>" class="form-control" name="customer-contact" maxlength="255" aria-invalid="false" disabled></td>
                </tr>
                <tr>
                    <th><b>Birthday</b></th>
                    <td style="padding: 0;"><input  placeholder="Customer Birthday" style="border-radius: 0;border: none;box-shadow: none;" type="text" id="customer-birthday" class="form-control" value="<?php echo date('d-m-Y',strtotime($customer->birthdate)) ?>" name="customer-birthday" maxlength="255" aria-invalid="false" disabled></td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>

</div>
<style>
    .radio label, .checkbox label {
        width: 100%;
        height: 100%;
    }
</style>