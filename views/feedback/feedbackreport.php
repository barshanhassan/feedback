<style>table#feedback_report {
    width: 100%;
}
.col-md-offset-2 {
    margin-left: 15.666667%;
}
.dt-buttons {
    margin-left: 1%;
	display: flex;
}
.feedback-search {
    margin-left: 1%;
	display: flex;
}

div#modepie {
    border: 2px solid black;
    margin-top: 2%;
}


</style>
<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Feedback */
/* @var $form yii\widgets\ActiveForm */
$this->title = Yii::t('app', 'Feedbacks Report');
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    .radio label, .checkbox label {
        width: 100%;
        height: 100%;
    }
    .dt-button{
        background-color: #337ab7;
        color: white;
        width: 60px;
        border: none;
        height: 30px;
        border-radius: 4px;
        margin-bottom: 10px;
    }

</style>
<script src="http://code.highcharts.com/highcharts.js"></script>
<script src="http://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/highcharts-3d.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://cdn.jsdelivr.net/canvg/1.4.0/rgbcolor.js"></script>
<script src="https://code.highcharts.com/modules/cylinder.js"></script>


    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
<?php
$flag  = 1;

if (isset($_GET['FeedbackSearch']['created_on']) && strpos($_GET['FeedbackSearch']['created_on'], ' - ') !== false ) {
    list($start_date, $end_date) = explode(' - ', $_GET['FeedbackSearch']['created_on']);
    $start_date = DateTime::createFromFormat('d/m/Y', $start_date);
    $start_date = $start_date->format('Y-m-d');
    $start_date = $start_date.' 00:00:00';

    $end_date = DateTime::createFromFormat('d/m/Y', $end_date);
    $end_date = $end_date->format('Y-m-d');
    $end_date = $end_date.' 23:59:59';
    $flag  = 1;

}




?>

<div class="feedback-form">

    <div class="row col-md-8 col-md-offset-2">
        <div class="col-lg-12">
            <?php echo $this->render('_search', ['model' => $searchModel]); ?>

            <?php if($flag==1) {
                $yes = 0;
                $no = 0;
                $count = 0;

                foreach ($dataProvider->models as $data)
                {
                   /* if($data['sms_promo']=='0')
                    {
                        $no++;

                    }else if($data['sms_promo']=='1')
                    {
                        $yes++;
                    }*/

                    $count ++;

                }

                ?>

            <table class="table table-bordered table-responsive table-condensed" id="feedback_report">
                <thead>

                <tr>
                    <th>&nbsp;</th>
                    <?php $revtype = \app\models\Reviewtype::find()->where('status=1')->all(); //echo '<pre>'; print_r($revtype);
                    foreach($revtype as $v){
                        ?>
                        <!--<th><?php /*$v->name*/?></th>-->
                        <?php
                    }
                    ?>
                    <td><i class="em em-slightly_smiling_face" style="width: 40px; height: 50px; background-image:url('<?= Yii::$app->homeUrl?>frontend/excellent.png');"></i></td>
                    <td><i class="em em-full_moon_with_face" style="width: 40px; height: 50px; background-image:url('<?= Yii::$app->homeUrl?>frontend/good.png');"></i></td>
                    <td><i class="em em-angry" style="width: 40px; height: 50px; background-image:url('<?= Yii::$app->homeUrl?>frontend/fair.png');"></i></td>
                </tr>
                <tr>
                    <th>Detail</th>
                    <th>Excelent</th>
                    <th>Good</th>
                    <th>Fair</th>
                </tr>
                </thead>
                <tbody>
                <?php $cat = \app\models\Category::find()->where('status=1')->all();
                foreach($cat as $k=>$v){
                    $subcat = \app\models\Subcategory::find()->where('categoryid = '.$v->id.' and status=1')->all();
                    ?>

                    <?php foreach($subcat as $d=>$p){ ?>
                        <tr>
                            <?php if($subcat[$d]['categoryid']!=$subcat[$d-1]['categoryid']){
                                ?>

                                <?php
                            } ?>
                            <td><?= $p->name ?></td>
                            <?php $revtype = \app\models\Reviewtype::find()->where('status=1')->all(); //echo '<pre>'; print_r($revtype);
                            $values = array();
                            foreach($revtype as $m){
                                ?>
                                <td style="text-align: center">
                                    <?php
                                    $cou = 0;
                                    foreach ($dataProvider->models as $data)
                                    {
                                        foreach ($data->feedBackDetail as $data2)
                                        {
                                            if ($data2->categoryid == $v->id)
                                            {
                                                if($data2->subcategoryid == $p->id )
                                                    if( $data2->reviewtypeid == $m->id)
                                                    {
                                                        $cou++;
                                                    }
                                            }
                                        }
                                    }

                                    ?>
                                   <?= $cou ?>
                                </td>
                                <?php
                                $val[] = $m->name;
                                $val[] = $cou;
                                $val[] = false;//array_push($results, $cou);
                                // $val[] = $i;
                                $i++;
                                $values[] = $val;
                                unset($val);
                            }
                            $results = json_encode($values);
                            ?>
                        </tr>
                    <?php } ?>
                    <?php
                }
                ?>
                <tr>
                    <td><b>Total Number Of Visitors</b></td>
                    <td style="text-align: center;" colspan="3">
                        <?= $count ?>
                    </td>
                    <td style="display: none"></td>
                    <td style="display: none"></td>

                </tr>
                </tbody>
            </table>
        </div>

                <!--<tr>
                    <th><b>Overall Visit Review (/5)</b></th>
                    <td style="padding: 0;text-align: center;">
                    </td>
                </tr>-->





        <?php } ?>

    </div>
</div>
<div class="col-md-12" id="modepie"></div>

</div>

<script>
    $(document).ready(function() {

        var settings={};
        settings.buttons = [
            {
                extend:'pdfHtml5',
                text:' PDF',
                customize: function (doc) {
                    doc.content[1].table.widths =
                        Array(doc.content[1].table.body[0].length + 1).join('*').split('');
                }
            }
        ];


        $('#feedback_report').DataTable({
            dom: 'Bfrtip',
            "bFilter": false,
            "paging": false,
            "bInfo": false,
            buttons: [
                {
                    extend: 'copy',
                },

                {
                    extend: 'csv',
                },
                {
                    extend: 'excel',
                },
                {
                    extend: 'pdf',
                    customize: function (doc) {
                        doc.content[1].table.widths =
                            Array(doc.content[1].table.body[0].length + 1).join('*').split('');
                        doc.content[1].margin = [ 100, 0, 100, 0 ]
                    }
                },
            ],

        } );

    } );

    var results = <?= $results ?>;
    Highcharts.setOptions({
        colors: ['#309b35', '#fdd21b', '#d3181e']
    });


    // Build the chart
    Highcharts.chart('modepie', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: 'Customer Feedback PieChart'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    },
                    connectorColor: 'silver'
                }
            }
        },
        series: [{
            name: 'Feedback',
            data: results,
        }],
        credits: {
            enabled: false
        },
        exporting: {
            buttons: {
                contextButton: {
                    menuItems: Highcharts.getOptions().exporting.buttons.contextButton.menuItems.filter(item => item !== 'openInCloud')
                }
            }
        }
    });

</script>

