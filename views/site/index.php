
<?php

/* @var $this yii\web\View */

$this->title = 'Feedback';
?>

<style>
    .step-wizard .progress {
        top: 0px !important;
    }

    * {
        -webkit-box-sizing:border-box;
        -moz-box-sizing:border-box;
        box-sizing:border-box;
    }

    *:before, *:after {
        -webkit-box-sizing: border-box;
        -moz-box-sizing: border-box;
        box-sizing: border-box;
    }

    .clearfix {
        clear:both;
    }

    .text-center {text-align:center;}
    a {
        color: tomato;
        text-decoration: none;
    }

    a:hover {
        color: #2196f3;
    }

    pre {
        display: block;
        padding: 9.5px;
        margin: 0 0 10px;
        font-size: 13px;
        line-height: 1.42857143;
        color: #333;
        word-break: break-all;
        word-wrap: break-word;
        background-color: #F5F5F5;
        border: 1px solid #CCC;
        border-radius: 4px;
    }

    .header {
        padding:20px 0;
        position:relative;
        margin-bottom:10px;

    }

    .header:after {
        content:"";
        display:block;
        height:1px;
        background:#eee;
        position:absolute;
        left:30%; right:30%;
    }

    .header h2 {
        font-size:3em;
        font-weight:300;
        margin-bottom:0.2em;
    }

    .header p {
        font-size:14px;
    }



    #a-footer {
        margin: 20px 0;
    }

    .new-react-version {
        padding: 20px 20px;
        border: 1px solid #eee;
        border-radius: 20px;
        box-shadow: 0 2px 12px 0 rgba(0,0,0,0.1);

        text-align: center;
        font-size: 14px;
        line-height: 1.7;
    }

    .new-react-version .react-svg-logo {
        text-align: center;
        max-width: 60px;
        margin: 20px auto;
        margin-top: 0;
    }

    .success-box {
        margin:15px 0;
    }

    .success-box img {
        margin-right:10px;
        display:inline-block;
        vertical-align:top;
    }
    .text-message{
        color: #fe195e;
    }

    .success-box > div {
        vertical-align:top;
        display:inline-block;
        color:#888;
    }

    /* Rating Star Widgets Style */
    .rating-stars ul {
        list-style-type:none;
        padding:0;
        list-style-position: inside;

        -moz-user-select:none;
        -webkit-user-select:none;
    }
    .rating-stars ul > li.star {
        display:inline-block;

    }

    /* Idle State of the stars */
    .rating-stars ul > li.star > i.fa {
        font-size:2.5em; /* Change the size of the stars */
        color:#ccc; /* Color on idle state */
    }

    /* Hover state of the stars */
    .rating-stars ul > li.star.hover > i.fa {
        color:#FFCC36;
    }

    /* Selected state of the stars */
    .rating-stars ul > li.star.selected > i.fa {
        color:#FF912C;
    }
    .star:after {
        content: "";
    }

    /* Customize the label (the container) */
    .checkbox-box {
        display: block;
        position: relative;
        padding-left: 35px;
        margin-bottom: 12px;
        cursor: pointer;
        font-size: 22px;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
    }

    /* Hide the browser's default radio button */
    .checkbox-box input {
        position: absolute;
        opacity: 0;
        cursor: pointer;
        height: 0;
        width: 0;
    }

    /* Create a custom radio button */
    .checkmark {
        position: absolute;
        top: -8px;
        left: 45%;
        height: 25px;
        width: 25px;
        background-color: #eee;
        border-radius: 50%;
        border: 1px solid #eb3235;
    }

    /* On mouse-over, add a grey background color */
    .checkbox-box:hover input ~ .checkmark {
        background-color: #ccc;
    }

    /* When the radio button is checked, add a blue background */
    .checkbox-box input:checked ~ .checkmark {
        background-color: #eb3235;
    }

    /* Create the indicator (the dot/circle - hidden when not checked) */
    .checkmark:after {
        content: "";
        position: absolute;
        display: none;
    }

    /* Show the indicator (dot/circle) when checked */
    .checkbox-box input:checked ~ .checkmark:after {
        display: block;
    }

    /* Style the indicator (dot/circle) */
    .checkbox-box .checkmark:after {
        top: 8px;
        left: 8px;
        width: 8px;
        height: 8px;
        border-radius: 50%;
        background: white;
    }

    form {

    }
    form label {
        color: #fe195e;

    }
    form label.error{
        font-size: .95rem;
        padding-top: 5px;
        float: left;

    }
    .error {
        background: #FFFF00;
    }
    .em-angry{


    }



    @media (max-width: 900px) {

        table.emoji-table{
            margin-left:200px !important;
        }

    }

    @media (min-width: 768px) {

        table.emoji-table{
            margin-left:300px !important;
        }

    }

    @media only screen and (max-width: 860px) {
        table.emoji-table{
            margin-left:170px !important;
        }
    }

    @media only screen and (max-width: 767px) {
        table.emoji-table{
            margin-left:150px !important;
        }
    }
    @media only screen and (max-width: 650px) {
        table.emoji-table{
            margin-left:80px !important;
        }
    }
    @media only screen and (max-width: 450px) {
        table.emoji-table{
            margin-left:0px !important;
        }
    }


</style>


<embed src="<?=Yii::$app->homeUrl?>frontend/elisyam/img/feedback_background_music.mp3"  autostart="true" preload="auto"  width="2" height="0">

<div class="widget-body">
    <div class="row flex-row justify-content-center">
        <div class="col-xl-10">
            <form class="needs-validation" novalidate id="commentForm" method="POST" action="<?=Yii::$app->homeUrl?>site/thankyou" class="form-horizontal" autocomplete="off">
                <input type="hidden" name="subcatradio" id="subcatradio" class="required"/>
                <input type="hidden" name="<?= Yii::$app->request->csrfParam; ?>" value="<?= Yii::$app->request->csrfToken; ?>" />
                <input type="hidden" name="outlet" value="<?=Yii::$app->user->identity->outlet_id?>"/>

                <div id="rootwizard">
                    <div class="step-container">
                        <div class="step-wizard" id="bar">
                            <div class="progress">
                                <div class="progressbar"></div>
                            </div>
                            <ul>
                                <li><a href="#tab1" data-toggle="tab"></a></li>
                                <?php
                                $cat = \app\models\Category::find()->where('status=1')->count();
                                //$revtype = \app\models\Reviewtype::find()->where('status=1')->count(); //echo '<pre>'; print_r($revtype);
                                $i=1;
                                for($i=1;$i<=$cat;$i++)
                                {?>

                               <?php } ?>

                                <li><a href="#tab2" data-toggle="tab"></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="tab-content">
                        <div class="tab-pane" id="tab1">
                            <h2 class="page-header-title">Customer Detail</h2>
                            <br>

                            <div class="widget-body">

                                <div class="form-group row d-flex align-items-center mb-5">
                                    <label class="col-sm-4 form-control-label d-flex justify-content-lg-end">Name *</label>
                                    <div class="col-sm-5">
                                        <input type="text" class="form-control required" id="name" name="name" placeholder="Enter your name">
                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center mb-5">
                                    <label class="col-sm-4 form-control-label d-flex justify-content-lg-end">Mobile # *</label>
                                    <div class="col-sm-5">
                                        <input type="text" class="form-control required" id ="phone" name ="phone" placeholder="Mobile Number"  onchange="getcust(this);" data-content="<?= \Yii::$app->urlManager->createUrl(['site/getcustdetail'])?>">
                                    </div>
                                </div>

                                <div class="form-group row d-flex align-items-center mb-5">
                                    <label class="col-sm-4 form-control-label d-flex justify-content-lg-end">Date of Birth</label>
                                    <div class="col-sm-5">
                                        <input type="text" class="form-control from" id ="dob"  name= "dob" placeholder="Date of Birth" >
                                    </div>
                                </div>

                                <?php $subcat = \app\models\Subcategory::find()->where('status=1')->all(); ?>
                                <?php foreach($subcat as $d=>$p){ ?>
                                    <br><h2 class="page-header-title"><?= $p->name?></h2><br><br>

                                       <!-- <table class="table mb-0">
                                            <thead>
                                            <tr>
                                                <td><i class="em em-slightly_smiling_face" style="width: 40px; height: 50px; background-image:url('<?= Yii::$app->homeUrl?>emoji/excellent.png');"></i></td>
                                                <td><i class="em em-full_moon_with_face" style="width: 40px; height: 50px; background-image:url('<?= Yii::$app->homeUrl?>emoji/good.png');"></i></td>
                                                <td><i class="em em-angry" style="width: 40px; height: 50px; background-image:url('<?= Yii::$app->homeUrl?>emoji/fair2.png');"></i></td>

                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr> -->
                                                <?php $revtype = \app\models\Reviewtype::find()->where('status=1')->all(); //echo '<pre>'; print_r($revtype);
                                                foreach($revtype as $m){
                                                    ?>
                                                   <!-- <td>
                                                        <label class="checkbox-box">
                                                            <input type="radio" id='subcatradio_<?php '1-'.$p->id.'-'.$m->id ?>' value="<?= '1-'.$p->id.'-'.$m->id ?>"  name="subcatradio[<?= $p->id ?>]">
                                                            <span class="checkmark"></span>

                                                        </label>
                                                    </td>-->

                                                    <?php
                                                }?>
                                           <!-- </tr>

                                            </tbody>
                                        </table> -->
                                        <div id="emojiContainer"></div>


                                        <div class="form-group row d-flex align-items-center mb-5">
                                            <label class="col-lg-4 form-control-label d-flex justify-content-lg-end"></label>
                                            <div class="col-lg-5">
                                                <div for="subcatradio" id="herror" class="error" style="color: red;text-align: left;font-size: 14px;margin-top: 15px" >Please Select one of above Option</div>
                                            </div>
                                        </div>
                                <?php } ?>

                            </div>
                        </div>


                        <?php
                        $j = 0;
                        //$cat = \app\models\Category::find()->where('status=1')->all();
                       // foreach($cat as $k=>$v){


                            $j++;
                            ?>

                        <div class="tab-pane" id="tab2">
                            <h2 class="page-header-title">Comment Something</h2>
                            <div style="padding:20px;">
                                <textarea id="detail" rows="7" name="remarks" placeholder="Write Some Comments Here........." class="form-control"></textarea>
                            </div>

                            <h2 class="page-header-title">Over All Rating</h2>
                            <br>
                            <div class="row">
                                <div class="col-12">
                                    <div class='rating-stars text-center'>
                                        <ul id='stars'>
                                            <li class='star' title='Poor' data-value='1'>
                                                <i class='fa fa-star fa-fw'></i>
                                            </li>
                                            <li class='star' title='Fair' data-value='2'>
                                                <i class='fa fa-star fa-fw'></i>
                                            </li>
                                            <li class='star' title='Good' data-value='3'>
                                                <i class='fa fa-star fa-fw'></i>
                                            </li>
                                            <li class='star' title='Excellent' data-value='4'>
                                                <i class='fa fa-star fa-fw'></i>
                                            </li>
                                            <li class='star' title='WOW!!!' data-value='5'>
                                                <i class='fa fa-star fa-fw'></i>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class='success-box'>
                                        <div class='clearfix'></div>
                                        <!--<img alt='tick image' width='32' src='data:image/svg+xml;utf8;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pgo8IS0tIEdlbmVyYXRvcjogQWRvYmUgSWxsdXN0cmF0b3IgMTkuMC4wLCBTVkcgRXhwb3J0IFBsdWctSW4gLiBTVkcgVmVyc2lvbjogNi4wMCBCdWlsZCAwKSAgLS0+CjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgdmVyc2lvbj0iMS4xIiBpZD0iTGF5ZXJfMSIgeD0iMHB4IiB5PSIwcHgiIHZpZXdCb3g9IjAgMCA0MjYuNjY3IDQyNi42NjciIHN0eWxlPSJlbmFibGUtYmFja2dyb3VuZDpuZXcgMCAwIDQyNi42NjcgNDI2LjY2NzsiIHhtbDpzcGFjZT0icHJlc2VydmUiIHdpZHRoPSI1MTJweCIgaGVpZ2h0PSI1MTJweCI+CjxwYXRoIHN0eWxlPSJmaWxsOiM2QUMyNTk7IiBkPSJNMjEzLjMzMywwQzk1LjUxOCwwLDAsOTUuNTE0LDAsMjEzLjMzM3M5NS41MTgsMjEzLjMzMywyMTMuMzMzLDIxMy4zMzMgIGMxMTcuODI4LDAsMjEzLjMzMy05NS41MTQsMjEzLjMzMy0yMTMuMzMzUzMzMS4xNTcsMCwyMTMuMzMzLDB6IE0xNzQuMTk5LDMyMi45MThsLTkzLjkzNS05My45MzFsMzEuMzA5LTMxLjMwOWw2Mi42MjYsNjIuNjIyICBsMTQwLjg5NC0xNDAuODk4bDMxLjMwOSwzMS4zMDlMMTc0LjE5OSwzMjIuOTE4eiIvPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8L3N2Zz4K'/>
                                        --><div class='text-message'></div>
                                        <div class='clearfix'></div>
                                    </div>
                                    <input type="hidden" name="myRate1" id="myRate1"/>
                                </div>
                            </div>
                        </div>
                        <ul class="pager wizard text-center" style="margin-top: 15px">
                            <li class="previous d-inline-block">
                                <a href="javascript:;" class="btn btn-secondary ripple">Previous</a>
                            </li>
                            <li class="next d-inline-block">
                                <a href="javascript:;" class="btn btn-gradient-01" id="next">Next</a>
                            </li>
                            <li class="next d-inline-block">
                                <input type="submit" class="finish btn btn-gradient-01" id="finish" style="display: none"/>
                            </li>
                        </ul>
                    </div>
            </form>
        </div>
    </div>
</div>
</div>


<script>

    $(document).ready(function(){
        $.noConflict();

        /* 1. Visualizing things on Hover - See next part for action on click */
        $('#stars li').on('mouseover', function(){
            var onStar = parseInt($(this).data('value'), 10); // The star currently mouse on

            // Now highlight all the stars that's not after the current hovered star
            $(this).parent().children('li.star').each(function(e){
                if (e < onStar) {
                    $(this).addClass('hover');
                }
                else {
                    $(this).removeClass('hover');
                }
            });

        }).on('mouseout', function(){
            $(this).parent().children('li.star').each(function(e){
                $(this).removeClass('hover');
            });
        });
        /* 2. Action to perform on click */
        $('#stars li').on('click', function(){
            var onStar = parseInt($(this).data('value'), 10); // The star currently selected
            var stars = $(this).parent().children('li.star');

            for (i = 0; i < stars.length; i++) {
                $(stars[i]).removeClass('selected');
            }

            for (i = 0; i < onStar; i++) {
                $(stars[i]).addClass('selected');
            }

            // JUST RESPONSE (Not needed)
            var ratingValue = parseInt($('#stars li.selected').last().data('value'), 10);
            var msg = "";
            if (ratingValue > 1) {
                msg = "Thanks! You rated this " + ratingValue + " Stars.";
            }
            else {
                msg = "We will improve ourselves. You rated this " + ratingValue + " stars.";
            }
            document.getElementById("myRate1").value = ratingValue;
            responseMessage(msg);
        });

        function responseMessage(msg) {
            $('.success-box').fadeIn(200);
            $('.success-box div.text-message').html(msg);
        }
        $('.from').datepicker({
            format: "dd/mm/yyyy",
            weekStart: 0,
            autoclose: true,
            todayHighlight: true
        });

        $.validator.addMethod('customphone', function (value, element) {
            return this.optional(element) || /^\d{11}$/.test(value);
        }, "Please Enter a 11 Digit Number");

        var $validator = $("#commentForm").validate({
            ignore: ".error",
            rules: {
                name: {
                    required: true,
                },
                phone: {
                    required: true,
                    number:true,
                    customphone:true,
                },
                email:{
                    required:false,
                    email:true,
                },
                subcatradio:{
                    required:true,

                }
            },
            messages: {
                name: {
                    required: "Please Enter Customer Name",
                },
                phone:{
                    required: "Please Enter Customer Mob#",
                    number:"Please Enter Valid Mob #"
                },
                email:{
                    email: "Please Enter Valid Email",
                },
                subcatradio:{
                    subcatradio:'Select one emoji',

                }
            },
            highlight: function (element, errorClass, validClass) {
                $(element).closest('tr').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).closest('tr').addClass(validClass).removeClass(errorClass);
            },
        });
        $("input[name*='subcatradio']").each(function() {
            $(this).rules('add', {
                required: true,
                messages: {
                    required:  ""
                },
            });
        });

        $('#rootwizard').bootstrapWizard({
            'tabClass': 'nav nav-pills',
            'onNext': function(f, a, d) {
                var $valid = $("#commentForm").valid();
                if(!$valid) {
                    $validator.focusInvalid();
                    return false;
                }},
            'onTabShow': function(f, a, d) {

                var c = a.find("li").length;
                var e = d + 1;
                var b = (e / c) * 100;
                $("#rootwizard .progressbar").css({
                    width: b + "%"
                });
                if(b==100)
                {
                    $('#next').hide();
                    $("#finish").show();
                }else
                {
                    $('#next').show();
                    $("#finish").hide();
                }
            }

        });
        $('#herror').hide();
        $('#next').on('click',function () {

            var cat = $('#subcatradio').val();
            if(cat==''){
                $('#herror').show();
            }else{
                $('#herror').hide();
            }
        });



    });

    function getcust(v){
        var val = $(v).val();
        var url=$(v).attr('data-content');
        //alert(url);
        $.ajax({
            type: "POST",
            url: url,
            data: {num:val},
            success: function(data){
                //var res = $.parseJSON(data);
                /*if(res.id==18){
                    //$('#feedback-customer_id').html(res.html);
                }
                else{
                    alert(res.msg);
                }*/
               // alert(data[1]);
            }
        });
    }

    $(function(){
        var emojis = ['poop','unlike', 'like', 'star', 'inlove'];
        $("#emojiContainer").emoji({
            opacity: 0.9,
            value: 0,
            width: '40px',
            emojis: ['<?= Yii::$app->homeUrl?>frontend/excellent.png','<?= Yii::$app->homeUrl?>frontend/good.png','<?= Yii::$app->homeUrl?>frontend/fair.png'],
            event: 'click',
            disabled: false,
            count: 0,
            color: '#eb3235',
           // animation: 'shake-opacity', //shake, shake-slow, shake-hard, shake-horizontal, shake-vertical, shake-rotate, shake-opacity, shake-crazy, shake-chunk
            //debug: true,

            callback: function (event, value) {
                   var val =  $("#emojiContainer").emoji("getvalue");

                    $('#subcatradio').val('1-1-'+val);
                    $('#herror').hide();

                $('.emoji-table tr').each(function() {

                    $.each(this.cells, function(){
                        console.log($(this).find('span').attr('value'));

                        var val = $(this).find('span').attr('value');
                        if(val == value)
                        {

                            $(this).find('span').css('font-size','40px');

                        }else {
                            $(this).find('span').css('font-size','40px');
                            $(this).find('span').css('opacity','0.3');
                        }
                    });

                });


            }

        });
    });


</script>