
<?php

/* @var $this yii\web\View */

$this->title = 'Thank you';
?>



<embed src="<?=Yii::$app->homeUrl?>frontend/elisyam/img/feedback_background_music.mp3" autostart="true" preload="auto" loop="true" width="2" height="0">

    <div class="container-fluid">
        <div class="row flex-row">
            <div class="col-12">
                <div class="widget has-shadow">
                    <div class="widget-header bordered no-actions d-flex align-items-center">
                        <h3 style="margin: 0 auto;">Dear Customer! <br><br><br> Thank you for Choosing Al-Khan Resturant.
                        Your Feedback is Value able for Us.</h3>
                    </div>
                    <div class="widget-body">
                        <a href="<?=Yii::$app->homeUrl ?>site/index">
                            <button type="button" class="btn btn-danger btn-lg mr-1 mb-2">Give Feedback Again</button>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Row -->
    </div>