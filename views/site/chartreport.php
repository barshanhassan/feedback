<style>

div#graph {
    border: 2px solid black;
}

div#modepie {
    border: 2px solid black;
}

div#visits {
    border: 2px solid black;
}

</style>

<?php
use yii\helpers\Html;
$this->title = Yii::t('app', 'Overall Chart Report');
$this->params['breadcrumbs'][] = $this->title;
?>


<script src="http://code.highcharts.com/highcharts.js"></script>
<script src="http://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/highcharts-3d.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://cdn.jsdelivr.net/canvg/1.4.0/rgbcolor.js"></script>
<script src="https://code.highcharts.com/modules/cylinder.js"></script>
<h1><?= Html::encode($this->title) ?></h1>
<div class="row">

    <?php
    $query = \app\models\Feedback::find()->joinWith('feedBackDetail')->all();
    $i=1;
    $cat = \app\models\Category::find()->where('status=1')->all();
    foreach($cat as $k=>$v) {
        $subcat = \app\models\Subcategory::find()->where('categoryid = '.$v->id.' and status=1')->all();
        foreach ($subcat as $d => $p) {
            $revtype = \app\models\Reviewtype::find()->where('status=1')->all();
            $values = array();
            foreach ($revtype as $m) {
                $cou = 0;

                foreach ($query as $data) {

                    foreach ($data->feedBackDetail as $data2) {

                        if ($data2->categoryid == $v->id) {
                            if ($data2->subcategoryid == $p->id)
                                if ($data2->reviewtypeid == $m->id) {
                                    $cou++;
                                }
                        }
                    }
                }

                $val[] = $m->name;
                $val[] = $cou;
                $val[] = false;//array_push($results, $cou);
                // $val[] = $i;
                $i++;
                $values[] = $val;
                unset($val);
            }
            $results = json_encode($values);
        }
    }
    $valuess =  Array();
    $overall = \app\models\Feedback::find()->all();
    //echo '<pre>';
   // echo print_r($overall);
    for ($j=1;$j<=5;$j++){
        $count=0;
        foreach ($overall as $ov){
            //echo $ov->overall_rating;
            if($ov->overall_rating==$j){
                $count+= $ov->overall_rating;
            }
        }
        $vall[] = $j.' Stars';
        $vall[] = $count/$j;
        $vall[] = false;

        $valuess[] = $vall;
        unset($vall);
        //echo $count.'\n';
    }
     $res = json_encode($valuess);
    $query =\app\models\Customer::find()->joinWith('feedbacks')->all();
   /* echo '<pre>';
    echo print_r($query);
    echo '</pre>';exit;*/

    $visits = Array();
    foreach ($query as $d)
    {
             $d->name ;

            $coun = 0;
            foreach ($d->feedbacks as $d2)
            {
                $coun++;
            }
            $vis[] = $d->name;
            $vis[] = $coun;
            $vis[] = false;
            $visits[]=$vis;
            unset($vis);
     }

     $vistt = json_encode($visits);
     ?>


    <div class="col-md-6" id="modepie"></div>
    <div class="col-md-6" id="graph"></div>
    <div class="col-md-12" id="visits"></div>


</div>
<script>
    var results = <?= $results ?>;
    var res = <?= $res ?>;
    var vistt = <?= $vistt ?>;


    Highcharts.setOptions({
        colors: ['#309b35', '#fdd21b', '#d3181e', '#ea8301', '#64E572', '#FF9655', '#FFF263','#6AF9C4']
    });


    // Build the chart
    Highcharts.chart('modepie', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },

        title: {
            text: 'Customer Feedback PieChart'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    allowPointSelect: true,
                    style: {
                    }
                   // connectorColor: 'silver'
                },

            },
        },
        series: [{
            name: 'Feedback',
            data: results,

        }],
        credits: {
            enabled: false
        },
        exporting: {
            buttons: {
                contextButton: {
                    menuItems: Highcharts.getOptions().exporting.buttons.contextButton.menuItems.filter(item => item !== 'openInCloud')
                }
            }
        }
    });

    Highcharts.setOptions({
        colors: ['#309b35', '#64E572', '#fdd21b', '#FD7CA7', '#d3181e', '#FF9655', '#FFF263',      '#6AF9C4']
    });

    Highcharts.chart('graph', {
        chart: {
            type: 'cylinder',
            options3d: {
                enabled: true,
                alpha: 0,
                beta: 10,
                depth: 80,
                viewDistance: 15
            }
        },
        xAxis: {
            tickInterval: 1,
            labels: {
                enabled: true,
                formatter: function() { return res[this.value][0];},
            }
        },
        yAxis: [{
            title: {
                text: 'Customers Counts'
            }
        }],
        title: {
            text: 'Customer Rating Chart'
        },
        plotOptions: {
            series: {
                depth: 25,
                colorByPoint: true
            }
        },
        series: [{
            name: 'Rating',
            data: res
            //showInLegend: false
        }],
        credits: {
            enabled: false
        },
        exporting: {
            buttons: {
                contextButton: {
                    menuItems: Highcharts.getOptions().exporting.buttons.contextButton.menuItems.filter(item => item !== 'openInCloud')
                }
            }
        }
    });

    Highcharts.setOptions({
        colors: [ '#2C4791', '#64E572', '#FF9655', '#FFF263','#6AF9C4']
    });

    Highcharts.chart('visits', {

        chart: {
            type: 'column'
            //styledMode: true
        },

        title: {
            text: 'Customers Visit Chart'
        },
        xAxis: {
            tickInterval: 1,
            labels: {
                enabled: true,
                formatter: function() { return vistt[this.value][0];},
            }
        },
        yAxis: [{
            className: 'highcharts-color-0',
            title: {
                text: 'Customers count'
            }
        }],

        plotOptions: {
            column: {
                borderRadius: 1
            }
        },

        series: [{
            name: 'Customers',
            data: vistt
        }],
        credits: {
            enabled: false
        },
        exporting: {
            buttons: {
                contextButton: {
                    menuItems: Highcharts.getOptions().exporting.buttons.contextButton.menuItems.filter(item => item !== 'openInCloud')
                }
            }
        }
    });

</script>
