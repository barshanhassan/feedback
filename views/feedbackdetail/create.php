<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Feedbackdetail */

$this->title = Yii::t('app', 'Create Feedbackdetail');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Feedbackdetails'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="feedbackdetail-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
