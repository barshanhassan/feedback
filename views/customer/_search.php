<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use DateTime;
use kartik\daterange\DateRangePicker;

/* @var $this yii\web\View */
/* @var $model app\models\CustomerSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="customer-search">

    <?php $form = ActiveForm::begin([
        'action' => ['get-customer-report'],
        'method' => 'get',
    ]); ?>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'outlet_id')
                ->dropDownList(
                    ArrayHelper::map(\app\models\Outlet::find()->asArray()->all(), 'id', 'name'),['prompt'=>'']
                )->label('Outlet'); ?>
        </div>
        <div class="col-md-6">

            <?php
            $addon = <<< HTML
<span class="input-group-addon">
    <i class="glyphicon glyphicon-calendar"></i>
</span>
HTML;



            echo '<label class="control-label">Date Range</label>';
            echo '<div class="drp-container">';
            echo DateRangePicker::widget([
                'model'=>$model,
                'attribute'=>'created_on',

                'convertFormat'=>true,

                'pluginOptions'=>[
                    'opens'=>'left',
                    'ranges' => [

                        "Today" => ["moment().startOf('day')", "moment()"],
                        "Yesterday" => ["moment().startOf('day').subtract(1,'days')", "moment().endOf('day').subtract(1,'days')"],
                        "Last 7 Days" => ["moment().startOf('day').subtract(6, 'days')", "moment()"],

                    ],

                    //'timePicker'=>true,
                    //'timePickerIncrement'=>05,
                    'locale'=>['format'=>'d/m/Y']
                ],
                'presetDropdown'=>false,
                'hideInput'=>true
            ]);
            echo '</div>'; ?>



        </div>
    </div>

    <?php $form->field($model, 'id') ?>

    <?php $form->field($model, 'name') ?>

    <?php $form->field($model, 'contact_no') ?>

    <?php $form->field($model, 'birthdate') ?>

    <?php $form->field($model, 'city') ?>

    <?php // echo $form->field($model, 'email') ?>

    <?php // echo $form->field($model, 'sms_promotion') ?>

    <?php // echo $form->field($model, 'link_to_facebook') ?>

    <?php // echo $form->field($model, 'facebook_link') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'outlet_id') ?>

    <?php // echo $form->field($model, 'created_on') ?>

    <?php // echo $form->field($model, 'created_by') ?>

    <?php // echo $form->field($model, 'updated_on') ?>

    <?php // echo $form->field($model, 'updated_by') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
