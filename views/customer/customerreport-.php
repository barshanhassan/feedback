<?php
/**
 * Created by PhpStorm.
 * User: MCE_Barshan
 * Date: 2/7/2019
 * Time: 4:30 PM
 */
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\daterange\DateRangePicker;
use yii\helpers\ArrayHelper;

$this->title = Yii::t('app', 'Customer Visit Report');
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    .dt-button{
        background-color: #337ab7;
        color: white;
        width: 60px;
        border: none;
        height: 30px;
        border-radius:4px ;
    }

</style>

<script src="http://code.highcharts.com/highcharts.js"></script>
<script src="http://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/highcharts-3d.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://cdn.jsdelivr.net/canvg/1.4.0/rgbcolor.js"></script>
<script src="https://code.highcharts.com/modules/cylinder.js"></script>

<h1><?= Html::encode($this->title) ?></h1>


<?php

$flag  = 1;

if (isset($_GET['CustomerSearch']['created_on']) && strpos($_GET['CustomerSearch']['created_on'], ' - ') !== false ) {
    list($start_date, $end_date) = explode(' - ', $_GET['CustomerSearch']['created_on']);
    $start_date = DateTime::createFromFormat('d/m/Y', $start_date);
    $start_date = $start_date->format('Y-m-d');
    $start_date = $start_date.' 00:00:00';
    $end_date = DateTime::createFromFormat('d/m/Y', $end_date);
    $end_date = $end_date->format('Y-m-d');
    $end_date = $end_date.' 23:59:59';
    $flag  = 1;

}
?>
<div class="row col-md-8 col-md-offset-2">
    <div class="col-lg-12">
        <?php echo $this->render('_search', ['model' => $searchModel]);?>
    </div>
</div>

<table id="customer_report" class="table table-striped table-bordered table-hover table-condensed" width="100%" cellspacing="0">
        <thead>
        <tr>
            <th>Customer Name</th>
            <th>Contact</th>
            <th>Birthday</th>
            <th>Visits</th>
            <th>Outlet Name</th>

        </tr>
        </thead>

        <tbody>

        <?php
        $visits = Array();
        foreach ($dataProvider->models as $data)
        {  ?>
            <tr>
            <td><?=  $data->name ?></td>
            <td><?=  $data->contact_no ?></td>

            <td><?=  $data->birthdate ?></td>

                <?php
                    $count = 0;
                foreach ($data->feedbacks as $data2)
                {
                    $count++;
                }
                $vis[] = $data->name;
                $vis[] = $count;
                $vis[] = false;
                $visits[]=$vis;
                unset($vis);
                    ?>
            <td><?php echo  $count ?></td>
                <?php $q= \app\models\Outlet::find()->where('id='.$data->outlet_id)->one()?>
                <td><?php echo  $q->name ?></td>
            </tr>

        <?php }
         $vistt = json_encode($visits);?>
        </tbody>

    </table>
<div class="col-md-12" id="visits"></div>

<script>

    $(document).ready(function() {
        $('#customer_report').DataTable( {
            dom: 'Bfrtip',
            buttons: [
                {
                    extend: 'copy',
                },

                {
                    extend: 'csv',
                },
                {
                    extend: 'excel',
                },
                {
                    extend: 'pdf',
                    customize: function (doc) {
                        doc.content[1].table.widths =
                            Array(doc.content[1].table.body[0].length + 1).join('*').split('');

                    }
                },
            ],
        } );
    } );

    Highcharts.setOptions({
        colors: ['#2C4791', '#fdd21b', '#d3181e', '#ea8301', '#64E572', '#FF9655', '#FFF263','#6AF9C4']
    });

    var vistt = <?= $vistt ?>;
    Highcharts.chart('visits', {
        chart: {
            type: 'column'
            //styledMode: true
        },

        title: {
            text: 'Customers visit Chart'
        },
        xAxis: {
            tickInterval: 1,
            labels: {
                enabled: true,
                formatter: function() { return vistt[this.value][0];},
            }
        },
        yAxis: [{
            className: 'highcharts-color-0',
            title: {
                text: 'Customers Visits'
            }
        }],

        plotOptions: {
            column: {
                borderRadius: 1
            }
        },

        series: [{
            name: 'Customers',
            data: vistt
        }],
        credits: {
            enabled: false
        },
        exporting: {
            buttons: {
                contextButton: {
                    menuItems: Highcharts.getOptions().exporting.buttons.contextButton.menuItems.filter(item => item !== 'openInCloud')
                }
            }
        }
    });


</script>




