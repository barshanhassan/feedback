<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Customer */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="customer-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-lg-4">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-lg-4">
            <?= $form->field($model, 'contact_no')->textInput() ?>
        </div>
        <div class="col-lg-4">
            <?= $form->field($model, 'birthdate')->textInput(['class' => "form-control datepicker"]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-4">
            <?= $form->field($model, 'city')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-lg-4">
            <?= $form->field($model, 'email')->textInput(['maxlength' => true])->input('email') ?>
        </div>
        <div class="col-lg-4">
            <?= $form->field($model, 'sms_promotion', [
                'template' => '<div class="col-xs-12 nopad">{label}</div> <div class="col-xs-9">{input}{error}{hint}</div>',])->radioList(array(1=>'Yes',0=>'No')); ?>

        </div>
    </div>

    <div class="row">
        <div class="col-lg-4">
            <?= $form->field($model, 'link_to_facebook', [
                'template' => '<div class="col-xs-12 nopad">{label}</div> <div class="col-xs-9">{input}{error}{hint}</div>',])->radioList(array(1=>'Yes',0=>'No')); ?>

        </div>
        <div class="col-lg-4">
            <?= $form->field($model, 'facebook_link')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-lg-4">
            <?php
            $disabled = '';
            $cond = '';
            if(Yii::$app->user->identity->outlet_id!=1){
                $model->outlet_id =Yii::$app->user->identity->outlet_id;
                $disable = 'disabled';
                $cond = 'outlet_id = '.Yii::$app->user->identity->outlet_id;
              //  echo $form->field($model, 'outlet_id')->hiddenInput(['value'=> Yii::$app->user->identity->outlet_id,'style'=>'display:none;'])->label(false);
            } ?>
            <?= $form->field($model, 'outlet_id')->dropDownList(
                ArrayHelper::map(\app\models\Outlet::find()->all(),'id','name'),
                ['prompt'=>'Select Outlet','class'=>'form-control',$disable=>'disabled']
            )?>
<?php if(Yii::$app->user->identity->outlet_id!=1){
                echo $form->field($model, 'outlet_id')->hiddenInput(['value'=> Yii::$app->user->identity->outlet_id,'style'=>'display:none;'])->label(false);
            }
            ?>
        </div>

    </div>
    <div class="row">
        <div class="col-lg-4">
            <?= $form->field($model, 'status', [
                'template' => '<div class="col-xs-12 nopad">{label}</div> <div class="col-xs-9">{input}{error}{hint}</div>',])->radioList(array(1=>'Active',0=>'InActive')); ?>
        </div>
        <div class="col-lg-4">
            <label class="col-lg-12" style=" height: 19px;"></label>
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
