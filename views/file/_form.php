<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\File */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="file-form">

    <?php $form = ActiveForm::begin('', 'post', ['enctype' => 'multipart/form-data']); ?>

    <?php
        echo $form->field($model, 'format')->dropDownList(['Mp4 for Home Page' => 'Mp4 for Home Page', 'Mp3 for Feedback Page' => 'Mp3 for Feedback Page'],['prompt'=>'Select Option.....']);
    ?>

    <?= $form->field($model, 'name')->fileInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
