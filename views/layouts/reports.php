<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => Html::img('@web/images/logo.jpg', ['alt'=>Yii::$app->name,'class' => 'img-responsive']),
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    if(!empty(Yii::$app->user->identity->name)&&isset(Yii::$app->user->identity->name)){
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
            ['label' => 'Home', 'url' => ['/site/index']],
            ['label' => 'Attendance', 'url' => ['/attendance/index']],
            //['label' => 'Mode Of Payments', 'url' => ['/mop/index']],
            //['label' => 'New Plans', 'url' => ['/newplan/index']],
            //['label' => 'Program', 'url' => ['/program/index']],
            ['label' => 'Registration', 'url' => ['/registration/index']],
            ['label' => 'Payments', 'url' => ['/payments/index']],
            ['label' => 'Take Payments', 'url' => ['/payments/viewpending']],
            [
                'label' => 'Reports',
                'items' => [
                    ['label' => 'Daily Sale Report', 'url' => ['/reports/dailysalereport']],
                    ['label' => 'Category Wise Report', 'url' => ['/reports/categorywisesalereport']],
                    ['label' => 'Attendence Report', 'url' => ['/reports/attendance']],
                    ['label' => 'Monthly Attendence Report', 'url' => ['/reports/monthlyattendance']],
                    ['label' => 'Pending Payment Report', 'url' => ['/reports/pendingpaymentreport']],
                    '<li class="divider"></li>',
                    ['label' => 'Active Customers Report', 'url' => ['/reports/activecustomer']],
                    ['label' => 'Suspended Customers Report', 'url' => ['/reports/suspendedcustomer']],
                    '<li class="divider"></li>',
                    ['label' => 'Categorywise Thermal Report', 'url' => ['/reports/categorywisethermalreport']],
                    //'<li class="divider"></li>',
                    //'<li class="dropdown-header">Dropdown Header</li>',
                    //['label' => 'Level 1 - Dropdown B', 'url' => '#'],
                ],
            ],
            [
                'label' => 'Settings',
                'items' => [

                    ['label' => 'Program', 'url' => ['/program/index']],
                    ['label' => 'New Plans', 'url' => ['/newplan/index']],
                    ['label' => 'Mode Of Payments', 'url' => ['/mop/index']],
                    '<li class="divider"></li>',

                    ['label' => 'Settings', 'url' => ['/settings/index']],
                    '<li class="divider"></li>',

                    ['label' => 'Add Users', 'url' => ['/users/index']],
                ],
            ],


            
            Yii::$app->user->isGuest ? (
                ['label' => 'Login', 'url' => ['/site/login']]
            ) : (
                '<li>'
                . Html::beginForm(['/site/logout'], 'get')
                . Html::submitButton(
                    'Logout (' . Yii::$app->user->identity->name . ')',
                    ['class' => 'btn btn-link logout']
                )
                . Html::endForm()
                . '</li>'
            )
        ],
    ]);
    }
    NavBar::end();
    
    ?>

    <div class="container-fluid">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; Maalik Creative Engineering, <?= date('Y') ?></p>

    <!--        <p class="pull-right">--><?//= Yii::powered() ?><!--</p>-->
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
