<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\FrontEndAppAsset;

FrontEndAppAsset::register($this);
$baseUrl = Yii::$app->homeUrl;
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <!-- Google Fonts -->
    <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js"></script>
    <script>
        WebFont.load({
            google: {"families":["Montserrat:400,500,600,700","Noto+Sans:400,700"]},
            active: function() {
                sessionStorage.fonts = true;
            }
        });
    </script>
    <!-- Favicon -->
    <link rel="apple-touch-icon" sizes="180x180" href="<?=$baseUrl?>frontend/elisyam/img/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?=$baseUrl?>frontend/elisyam/img/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?=$baseUrl?>frontend/elisyam/img/favicon-16x16.png">
    <!-- Tweaks for older IEs--><!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
    <script src="<?=$baseUrl?>frontend/elisyam/vendors/js/base/jquery.min.js"></script>
    <?php $this->head() ?>
</head>
<body class="bg-fixed-04">
<?php $this->beginBody() ?>

<!-- Begin Preloader -->
<div id="preloader">
    <div class="canvas">
        <img src="<?=Yii::$app->homeUrl?>frontend/elisyam/img/logo.png" alt="logo" class="loader-logo">
        <div class="spinner"></div>
    </div>
</div>
<!-- End Preloader -->

<!-- Begin Container -->
<div class="container-fluid h-100 overflow-y">
    <!--new-->
    <div class="row">
        <div class="col-1 ">
            <a href="<?=Yii::$app->homeUrl?>site/home">Home</a>
            <?php if(\Yii::$app->user->identity->rights=='admin'){
                echo  Html::a('Reports',['feedback/index']);
            }?>
            <a href="<?=Yii::$app->homeUrl?>site/logout">Logout</a>
        </div>



        <div class="col-11">
            <div class="coming-soon mx-auto">
                <div class="logo-centered">
                    <a href="#">
                        <img src="<?=Yii::$app->homeUrl?>frontend/logo.png" alt="logo" style="height: 109px; width: 205px;margin-top: -28px;">
                    </a>
                </div>

                <?= $content ?>
            </div>
        </div>


    </div>
    <!--end new-->


    <!-- End Row -->
</div>
<!-- End Container -->



<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
