<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;

\app\assets\LoginAsset::register($this);
$baseUrl = Yii::$app->homeUrl;
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <!-- Google Fonts -->
    <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js"></script>
    <script>
        WebFont.load({
            google: {"families":["Montserrat:400,500,600,700","Noto+Sans:400,700"]},
            active: function() {
                sessionStorage.fonts = true;
            }
        });
    </script>
    <!-- Favicon -->
    <link rel="apple-touch-icon" sizes="180x180" href="<?=$baseUrl?>elisyam/img/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?=$baseUrl?>elisyam/img/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?=$baseUrl?>elisyam/img/favicon-16x16.png">
    <!-- Tweaks for older IEs--><!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
    <script src="<?=$baseUrl?>elisyam/vendors/js/base/jquery.min.js"></script>
    <?php $this->head() ?>
	<style>
	#myVideo {
  position: fixed;
  right: 0;
  bottom: 0;
  min-width: 100%; 
  min-height: 100%;
}
.logo-left1 {
    width: 148px;
	    background: white;
    padding: 7px;
}
.logo-left1 img {
    width: 136px;
    margin-bottom: 0px;
}

   
    font-size: .9rem;
    padding: 3rem 3rem;
    display: block;
    width: 400px;
    max-width: 400px;
   z-index: 2;
   
}
.h12-100 {
    height: 89%!important;
}
</style>
</head>
<body class="bg-fixed-02">
<video autoplay loop id="myVideo">
  <source src="<?=Yii::$app->homeUrl?>elisyam/img/video-intro.mp4" type="video/mp4">
</video>

<?php $this->beginBody() ?>

<!-- Begin Preloader -->
<div id="preloader">
    <div class="canvas">
        <img src="<?=Yii::$app->homeUrl?>elisyam/img/logo.png" alt="logo" class="loader-logo">
        <div class="spinner"></div>
    </div>
</div>
<!-- End Preloader -->

<!-- Begin Container -->
<div class="container-fluid h-100 overflow-y">
	<div class="row">
        <div class="col-12">
		<div class="logo-left1">

                        <img src="<?= Yii::$app->homeUrl?>elisyam/img/logo.png" alt="logo">

                </div>
		
		</div>
	</div>
		
    <div class="row flex-row h12-100">
        <div class="col-12 my-auto">
			<div class="button-box  mx-auto">
			<!--<button type="button" class="btn btn-outline-light">Click to Give Feedback !</button>
			<button type="button" class="btn-outline-light" data-url="<?= \Yii::$app->urlManager->createUrl(['site/index']) ?>">Click to Give Feedback !</button>-->
			<?= Html::a(Yii::t('app', 'Click to Give Feedback !'), ['index'], ['class' => 'btn-outline-light']) ?>
			</div>
		
          <!--  <div class="password-form mx-auto">
                <div class="logo-centered">
					

                        <img src="<?= Yii::$app->homeUrl?>elisyam/img/logo.png" alt="logo">

                </div>
				this is content area-->
               <?= $content ?>
            </div>
        </div>
        <!-- End Col -->
    </div>
    <!-- End Row -->
</div>
<!-- End Container -->
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
