<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;

\app\assets\LoginAsset::register($this);
$baseUrl = Yii::$app->homeUrl;
?>
<style>

    .bg-fixed-02{
        margin-top: -40px;
    }






</style>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <!-- Google Fonts -->
    <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js"></script>
    <script>
        WebFont.load({
            google: {"families":["Montserrat:400,500,600,700","Noto+Sans:400,700"]},
            active: function() {
                sessionStorage.fonts = true;
            }
        });
    </script>
    <!-- Favicon -->
    <link rel="apple-touch-icon" sizes="180x180" href="<?=$baseUrl?>frontend/elisyam/img/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?=$baseUrl?>frontend/elisyam/img/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?=$baseUrl?>frontend/elisyam/img/favicon-16x16.png">
    <!-- Tweaks for older IEs--><!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
    <script src="<?=$baseUrl?>frontend/elisyam/vendors/js/base/jquery.min.js"></script>
    <?php $this->head() ?>
</head>
<body class="bg-fixed-02">
<?php $this->beginBody() ?>

<!-- Begin Preloader -->
<div id="preloader">
    <div class="canvas">
        <img src="<?=Yii::$app->homeUrl?>frontend/elisyam/img/logo.png" alt="logo" class="loader-logo">
        <div class="spinner"></div>
    </div>
</div>
<!-- End Preloader -->

<!-- Begin Container -->
<div class="container-fluid h-100 overflow-y">
    <div class="row flex-row h-100">
        <div class="col-12 my-auto">
            <div class="password-form mx-auto">
                <div class="logo-centered" style="width: 70%;">

                        <img src="<?= Yii::$app->homeUrl?>frontend/elisyam/img/logo.png" alt="logo">

                </div>
               <?= $content ?>
            </div>
        </div>
        <!-- End Col -->
    </div>
    <!-- End Row -->
</div>
<!-- End Container -->
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
