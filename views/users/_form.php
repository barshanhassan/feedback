<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\users */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="users-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-xs-6">
            <?= $form->field($model, 'name', [
                'template' => '<div class="col-xs-3">{label}</div> <div class="col-xs-9">{input}{error}{hint}</div>',
            ]) ?>
        </div>
        <div class="col-xs-6">
            <?= $form->field($model, 'outlet_id',[
                'template' => '<div class="col-xs-3">{label}</div> <div class="col-xs-9">{input}{error}{hint}</div>',
            ])->dropDownList(
                ArrayHelper::map(\app\models\Outlet::find()->asArray()->all(), 'id', 'name'),(['prompt' => '---Select Data---'])
            ) ?></div>
    </div>

    <div class="row">
        <!--<div class="col-xs-6">
            <?/*= $form->field($model, 'authKey', [
                'template' => '<div class="col-xs-3">{label}</div> <div class="col-xs-9">{input}{error}{hint}</div>',
            ]) */?>
        </div>-->
        <div class="col-xs-6">
            <?= $form->field($model, 'user', [
                'template' => '<div class="col-xs-3">{label}</div> <div class="col-xs-9">{input}{error}{hint}</div>',
            ]) ?></div>
        <div class="col-xs-6">
            <?= $form->field($model, 'password', [
                'template' => '<div class="col-xs-3">{label}</div> <div class="col-xs-9">{input}{error}{hint}</div>',
            ])->passwordInput() ?>
        </div>
    </div>

    <div class="row">

        <div class="col-xs-6">
            <?= $form->field($model, 'status', [
                'template' => '<div class="col-xs-3 nopad">{label}</div> <div class="col-xs-9">{input}{error}{hint}</div>',])->radioList(array(1=>'Active',0=>'InActive')); ?>
        </div>
        <div class="col-xs-6">
            <?= $form->field($model, 'rights',[
                'template' => '<div class="col-xs-3">{label}</div> <div class="col-xs-9">{input}{error}{hint}</div>',
            ])->dropdownList(['admin' => 'admin', 'user' => 'user'], ['prompt' => '---Select Data---','id'=>'rightss']) ?></div>

    </div>

    <!--<div class="row">
        <div class="col-xs-6">
            <?/*= $form->field($model, 'created_on', [
                'template' => '<div class="col-xs-3">{label}</div> <div class="col-xs-9">{input}{error}{hint}</div>',
            ]) */?>
        </div>
        <div class="col-xs-6">
            <?/*= $form->field($model, 'updated_by', [
                'template' => '<div class="col-xs-3">{label}</div> <div class="col-xs-9">{input}{error}{hint}</div>',
            ]) */?></div>
    </div>-->

    <!--<div class="row">
        <div class="col-xs-6">
            <?/*= $form->field($model, 'updated_on', [
                'template' => '<div class="col-xs-3">{label}</div> <div class="col-xs-9">{input}{error}{hint}</div>',
            ]) */?>
        </div>
        </div>-->
  

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>