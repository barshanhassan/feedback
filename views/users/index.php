<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\usersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Users';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="users-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Users', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

          //  'id',
            'name',
            'email:email',
           // 'authKey',
            'user',
            [

                'attribute' => 'rights',
                'value' => function ($model) {
                    if($model->rights=='admin'){
                        return 'Admin';
                    }
                    elseif ($model->rights=='user'){
                        return 'User';
                    }

                    //return $model->rights ==3 ? 'Front Office' :'Admin';
                    //return $model->rights ==2 ? 'Back Office' : 'Admin';
                },
            ],
            // 'password',
            // 'status',
            // 'rights',
            // 'created_by',
            // 'created_on',
            // 'updated_by',
            // 'updated_on',

            ['class' => 'yii\grid\ActionColumn',
                'header'=>'Actions',
                'visibleButtons' => [


                    'delete' => function ($model) {
                        return $model->id == 0;
                    },

                ]
            ],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
