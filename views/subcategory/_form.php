<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Subcategory */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="subcategory-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-lg-4">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-lg-4">
            <?= $form->field($model, 'categoryid', [
                'template' => '<div class="col-xs-12 nopad">{label}</div> <div class="col-xs-12">{input}{error}{hint}</div>',
            ])->dropDownList(
                ArrayHelper::map(\app\models\Category::find()->all(),'id','cat_name'),
                ['prompt'=>'Select Category','class'=>'form-control']
            )?>
        </div>
        <div class="col-lg-4">
            <?= $form->field($model, 'status', [
                'template' => '<div class="col-xs-12 nopad">{label}</div> <div class="col-xs-9">{input}{error}{hint}</div>',])->radioList(array(1=>'Active',0=>'InActive')); ?>
        </div>
    </div>






    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
