-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 30, 2020 at 06:26 PM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `feedback_alkhan`
--

-- --------------------------------------------------------

--
-- Table structure for table `auth_assignment`
--

CREATE TABLE IF NOT EXISTS `auth_assignment` (
  `item_name` varchar(64) NOT NULL,
  `user_id` varchar(64) NOT NULL,
  `created_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `auth_item`
--

CREATE TABLE IF NOT EXISTS `auth_item` (
  `name` varchar(64) NOT NULL,
  `type` smallint(6) NOT NULL,
  `description` text,
  `rule_name` varchar(64) DEFAULT NULL,
  `data` blob,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `auth_item_child`
--

CREATE TABLE IF NOT EXISTS `auth_item_child` (
  `parent` varchar(64) NOT NULL,
  `child` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `auth_rule`
--

CREATE TABLE IF NOT EXISTS `auth_rule` (
  `name` varchar(64) NOT NULL,
  `data` blob,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE IF NOT EXISTS `category` (
`id` int(11) NOT NULL,
  `cat_name` varchar(100) NOT NULL,
  `detail` varchar(100) DEFAULT NULL,
  `status` int(1) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `cat_name`, `detail`, `status`) VALUES
(1, 'Customer Review', 'Customer Review', 1),
(4, 'food', 'food quality', 1),
(5, 'staff', 'staff detail', 1);

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE IF NOT EXISTS `customer` (
`id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `contact_no` varchar(111) DEFAULT NULL,
  `birthdate` date DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `sms_promotion` int(1) DEFAULT NULL,
  `link_to_facebook` int(1) DEFAULT NULL,
  `facebook_link` varchar(100) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `outlet_id` int(11) NOT NULL,
  `created_on` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_on` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`id`, `name`, `contact_no`, `birthdate`, `city`, `email`, `sms_promotion`, `link_to_facebook`, `facebook_link`, `status`, `outlet_id`, `created_on`, `created_by`, `updated_on`, `updated_by`) VALUES
(44, 'barshan', '03069091792', NULL, NULL, NULL, NULL, NULL, NULL, 1, 4, '2019-02-12 14:45:51', 3, '2019-02-14 13:31:29', NULL),
(45, 'Zain Kazmi', '03226506505', '1990-03-11', NULL, NULL, NULL, NULL, NULL, 1, 4, '2019-02-12 16:31:50', 3, '2019-02-12 16:31:50', NULL),
(46, 'Zuhair Hassan', '03429275005', '2018-05-23', NULL, NULL, NULL, NULL, NULL, 1, 4, '2019-02-14 13:28:22', 3, '2019-02-14 13:28:22', NULL),
(47, 'jjhjhjh', '76767676767', '2020-03-26', NULL, NULL, NULL, NULL, NULL, 1, 4, '2020-03-14 12:08:45', 3, '2020-03-14 12:08:48', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `feedback`
--

CREATE TABLE IF NOT EXISTS `feedback` (
`id` int(11) NOT NULL,
  `outlet_id` int(11) DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `overall_rating` varchar(4) NOT NULL DEFAULT '0',
  `sms_promo` int(11) DEFAULT NULL,
  `remarks` varchar(255) DEFAULT NULL,
  `created_on` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_on` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `feedback`
--

INSERT INTO `feedback` (`id`, `outlet_id`, `customer_id`, `overall_rating`, `sms_promo`, `remarks`, `created_on`, `created_by`, `updated_on`, `updated_by`) VALUES
(52, 4, 44, '4', 0, 'not bad', '2019-02-12 14:45:51', 3, NULL, NULL),
(53, 4, 44, '3', 0, '', '2019-02-12 14:53:29', 3, NULL, NULL),
(54, 4, 45, '4', 0, 'Great Environment!', '2019-02-12 16:31:52', 3, NULL, NULL),
(55, 4, 44, '2', 0, 'nnnnnnn', '2019-02-13 15:15:32', 3, NULL, NULL),
(56, 4, 46, '2', 0, '', '2019-02-14 13:28:22', 3, NULL, NULL),
(57, 4, 44, '2', 0, 'testing purpose', '2019-02-14 13:31:00', 3, NULL, NULL),
(58, 4, 44, '2', 0, '', '2019-02-14 13:31:29', 3, NULL, NULL),
(59, 4, 47, '', 0, '', '2020-03-14 12:08:45', 3, NULL, NULL),
(60, 4, 47, '', 0, '', '2020-03-14 12:08:48', 3, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `feedbackdetail`
--

CREATE TABLE IF NOT EXISTS `feedbackdetail` (
`id` int(11) NOT NULL,
  `feedback_id` int(11) NOT NULL,
  `categoryid` int(11) NOT NULL,
  `subcategoryid` int(11) NOT NULL,
  `reviewtypeid` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=573 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `feedbackdetail`
--

INSERT INTO `feedbackdetail` (`id`, `feedback_id`, `categoryid`, `subcategoryid`, `reviewtypeid`) VALUES
(565, 52, 1, 1, 2),
(566, 53, 1, 1, 1),
(567, 54, 1, 1, 2),
(568, 55, 1, 1, 3),
(569, 56, 1, 1, 1),
(570, 57, 1, 1, 1),
(571, 58, 1, 1, 1),
(572, 59, 1, 1, 3);

-- --------------------------------------------------------

--
-- Table structure for table `outlet`
--

CREATE TABLE IF NOT EXISTS `outlet` (
`id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `address` varchar(255) NOT NULL,
  `contactno` varchar(100) NOT NULL,
  `email` varchar(100) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `outlet`
--

INSERT INTO `outlet` (`id`, `name`, `address`, `contactno`, `email`, `status`) VALUES
(4, 'Al-Khan', ' Alkhan, Thoker Branch Thokar Niaz Baig, Main Raiwind Road, Lahore', '042-35312906', 'alkhan@hotmail.com', 1),
(5, 'Test Branch', 'PHA, G-7/1, Islamabad', '0512204040', 'test@hotmail.com', 1);

-- --------------------------------------------------------

--
-- Table structure for table `reviewtype`
--

CREATE TABLE IF NOT EXISTS `reviewtype` (
`id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reviewtype`
--

INSERT INTO `reviewtype` (`id`, `name`, `status`) VALUES
(1, 'Excellent', 1),
(2, 'Good', 1),
(3, 'Fair', 1);

-- --------------------------------------------------------

--
-- Table structure for table `sms_text`
--

CREATE TABLE IF NOT EXISTS `sms_text` (
`id` int(11) NOT NULL,
  `text` varchar(10000) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sms_text`
--

INSERT INTO `sms_text` (`id`, `text`) VALUES
(2, '   Dear Customer!\r\nThank you for visiting Al-khan restaurant and giving us your valuable feedback.\r\nContact# 051-2204040\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `subcategory`
--

CREATE TABLE IF NOT EXISTS `subcategory` (
`id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `categoryid` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subcategory`
--

INSERT INTO `subcategory` (`id`, `name`, `categoryid`, `status`) VALUES
(1, 'Please rate your experience today!', 1, 1),
(14, 'how was a food', 4, 1),
(15, 'how was food taste', 4, 1),
(16, 'how was staff behaviour', 5, 1),
(17, 'how was staff', 5, 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
`id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `email` varchar(100) DEFAULT NULL,
  `authKey` varchar(10) DEFAULT NULL,
  `user` varchar(20) NOT NULL,
  `password` varchar(50) NOT NULL,
  `status` int(11) DEFAULT NULL,
  `rights` varchar(50) DEFAULT NULL,
  `outlet_id` int(11) DEFAULT NULL,
  `landing_page` varchar(1000) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_on` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_on` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `authKey`, `user`, `password`, `status`, `rights`, `outlet_id`, `landing_page`, `created_by`, `created_on`, `updated_by`, `updated_on`) VALUES
(2, 'feedback', 'mce@gmail.com', NULL, 'feedback', 'feedback', 1, 'user', 4, NULL, NULL, NULL, NULL, NULL),
(3, 'admin', 'mce@maaliksoft.com', NULL, 'admin', 'admin123', 1, 'admin', 4, NULL, NULL, '2019-02-07 00:00:00', NULL, NULL),
(4, 'manager', 'manager@gmail.com', NULL, 'manager', 'manager', 1, 'admin', 4, NULL, NULL, '2019-02-11 01:15:03', NULL, NULL),
(5, 'manager2', 'mce@gmail.com', NULL, 'manager2', 'manager2', 1, 'admin', 5, NULL, NULL, '2019-02-12 11:30:56', NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `auth_assignment`
--
ALTER TABLE `auth_assignment`
 ADD PRIMARY KEY (`item_name`,`user_id`);

--
-- Indexes for table `auth_item`
--
ALTER TABLE `auth_item`
 ADD PRIMARY KEY (`name`), ADD KEY `rule_name` (`rule_name`), ADD KEY `type` (`type`);

--
-- Indexes for table `auth_item_child`
--
ALTER TABLE `auth_item_child`
 ADD PRIMARY KEY (`parent`,`child`), ADD KEY `child` (`child`);

--
-- Indexes for table `auth_rule`
--
ALTER TABLE `auth_rule`
 ADD PRIMARY KEY (`name`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
 ADD PRIMARY KEY (`id`), ADD KEY `outlet_id` (`outlet_id`);

--
-- Indexes for table `feedback`
--
ALTER TABLE `feedback`
 ADD PRIMARY KEY (`id`), ADD KEY `outlet_id` (`outlet_id`), ADD KEY `customer_id` (`customer_id`);

--
-- Indexes for table `feedbackdetail`
--
ALTER TABLE `feedbackdetail`
 ADD PRIMARY KEY (`id`), ADD KEY `feedback_id` (`feedback_id`), ADD KEY `categoryid` (`categoryid`), ADD KEY `subcategoryid` (`subcategoryid`), ADD KEY `reviewtypeid` (`reviewtypeid`);

--
-- Indexes for table `outlet`
--
ALTER TABLE `outlet`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reviewtype`
--
ALTER TABLE `reviewtype`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sms_text`
--
ALTER TABLE `sms_text`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subcategory`
--
ALTER TABLE `subcategory`
 ADD PRIMARY KEY (`id`), ADD KEY `categoryid` (`categoryid`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`id`), ADD KEY `outlet_id` (`outlet_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=48;
--
-- AUTO_INCREMENT for table `feedback`
--
ALTER TABLE `feedback`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=61;
--
-- AUTO_INCREMENT for table `feedbackdetail`
--
ALTER TABLE `feedbackdetail`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=573;
--
-- AUTO_INCREMENT for table `outlet`
--
ALTER TABLE `outlet`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `reviewtype`
--
ALTER TABLE `reviewtype`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `sms_text`
--
ALTER TABLE `sms_text`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `subcategory`
--
ALTER TABLE `subcategory`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `auth_assignment`
--
ALTER TABLE `auth_assignment`
ADD CONSTRAINT `auth_assignment_ibfk_1` FOREIGN KEY (`item_name`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `auth_item`
--
ALTER TABLE `auth_item`
ADD CONSTRAINT `auth_item_ibfk_1` FOREIGN KEY (`rule_name`) REFERENCES `auth_rule` (`name`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `auth_item_child`
--
ALTER TABLE `auth_item_child`
ADD CONSTRAINT `auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `customer`
--
ALTER TABLE `customer`
ADD CONSTRAINT `customer_ibfk_1` FOREIGN KEY (`outlet_id`) REFERENCES `outlet` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `feedback`
--
ALTER TABLE `feedback`
ADD CONSTRAINT `feedback_ibfk_1` FOREIGN KEY (`outlet_id`) REFERENCES `outlet` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `feedback_ibfk_2` FOREIGN KEY (`customer_id`) REFERENCES `customer` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `feedbackdetail`
--
ALTER TABLE `feedbackdetail`
ADD CONSTRAINT `feedbackdetail_ibfk_1` FOREIGN KEY (`feedback_id`) REFERENCES `feedback` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `feedbackdetail_ibfk_2` FOREIGN KEY (`categoryid`) REFERENCES `category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `feedbackdetail_ibfk_3` FOREIGN KEY (`subcategoryid`) REFERENCES `subcategory` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `feedbackdetail_ibfk_4` FOREIGN KEY (`reviewtypeid`) REFERENCES `reviewtype` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `subcategory`
--
ALTER TABLE `subcategory`
ADD CONSTRAINT `subcategory_ibfk_1` FOREIGN KEY (`categoryid`) REFERENCES `category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`outlet_id`) REFERENCES `outlet` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
